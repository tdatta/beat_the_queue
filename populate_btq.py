# The testing script
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'btq.settings')
from django.contrib.auth.models import Group

import django
django.setup()

from btq.models import *


def add_item(item_desc):
    return Item.objects.get_or_create(name=item_desc)[0]


def add_menu(item, i_type, price, size="regular", vendor=None, desc=""):
    m = Menu.objects.get_or_create(
        item=item,
        item_type=i_type,
        description=desc,
        price=price,
        size=size,
        vendor=vendor)
    return m[0]


def add_order(item, quantity, user, vendor):
    order = Order.objects.get_or_create(
        item_id=item,
        quantity=quantity,
        user=user,
        vendor=vendor)
    return order[0]


def add_userfav(usr, vendor):
    return UserFavourite.objects.get_or_create(user=usr, vendor=vendor)[0]


def add_group(gp_name):
    return Group.objects.get_or_create(name=gp_name)[0]


def add_user(username, group):
    user = User.objects.get_or_create(username=username)[0]
    group.user_set.add(user)
    return user


def populate():
    """
    the main population script
    """
    user_group = add_group("is_user")
    vendor_group = add_group("is_vendor")
    # create vendors
    jaggies = add_user(username="jaggies", group=vendor_group)
    starbux = add_user(username="starbux", group=vendor_group)

    # create user
    tanmay = add_user("tanmay", user_group)
    manu = add_user("manu", user_group)
    akku = add_user("akku", user_group)

    # create items
    latte = add_item("latte")
    chapati = add_item("chapati")
    barfi = add_item("barfi")

    # add menu
    # jaggies ka menu
    barfi_jaggies = add_menu(barfi,
                             "Mithaiya",
                             3.4,
                             size="regular",
                             vendor=jaggies,
                             desc="Kaju barfi")
    chapati_jaggies = add_menu(chapati,
                               "rotiya",
                               1.2,
                               vendor=jaggies)
    coffee_jaggies = add_menu(latte,
                              "kaafi",
                              2.4,
                              vendor=jaggies)
    # starbux ka menu
    regular_latte_sbux = add_menu(
        latte,
        "house_specials",
        6.1, vendor=starbux,
        desc="our most famous drink")
    ventri_laate_sbux = add_menu(latte,
                                 "house_specials",
                                 7, "ventri",
                                 starbux)


if __name__ == '__main__':
    populate()
