# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Web-server for beat the queue app
* Beta
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Install python (suggested is anaconda installed on windows)
* Run python manage.py syncdb
* Go to the root folder and run python populate.py 
* Dependancies:
1. json
2. datetime

* Database: sqlite
* How to run tests--> To add test nothing yet
* To run 
1. Root run python manage.py runserver

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* tanmay.datta86@gmail.com
* manu.datta@gmail.com