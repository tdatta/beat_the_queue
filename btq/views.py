from django.http import HttpResponse
from django.template import RequestContext
from django.template.response import TemplateResponse
from django.shortcuts import render_to_response, render
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
import json


# def index(request):
#     return render_to_response('index.html')

@login_required
def get_user_favs(request):
    user = request.user
    fav_list = user.ke_fav.all()
    json_fav_list = list()
    for fl in fav_list:
        json_fav_list.append(
            {'id': fl.vendor_id_id,
             'vendor': fl.vendor_id.username})
    data = json_fav_list
    # dynatable specific
    # data = {'records':d}
    # data["queryRecordCount"] = count
    # data["totalRecordCount"] = count
    # end dynatable
    # data = serializers.serialize('json',data)
    # print data
    return HttpResponse(json.dumps(data))


def get_menu(request, vendor):
    vendor_user = User.objects.filter(username=vendor)[0]
    menu_list = list()
    for item in vendor_user.vendor_ka_menu.all():
        menu_list.append({
            'item': item.item.name,
            'type': item.item_type,
            'price': item.price,
            'size': item.size,
            'description': item.description
            })
    return HttpResponse(json.dumps(menu_list))


def is_logged(request):
    return HttpResponse(json.dumps({"logged_in": "no"})) \
        if request.user.is_anonymous() \
        else HttpResponse(json.dumps({"logged_in": "yes"}))


def home(request):
    return TemplateResponse(request, 'index.html')


def index(request, return_type='JSON'):
    user = request.user
    fav_list = user.ke_fav.all()
    json_fav_list = list({'nothing': None})
    for fl in fav_list:
        json_fav_list.append({'vendor': fl.vendor_id.username})
    data = json_fav_list
    # dynatable specific
    # data = {'records':d}
    # data["queryRecordCount"] = count
    # data["totalRecordCount"] = count
    # end dynatable
    # data = serializers.serialize('json',data)
    # print data
    if return_type.upper() == 'JSON':
        return HttpResponse(json.dumps(data))
        # return HttpResponse(data, content_type="application/json")
    else:
        print "here"
        return render_to_response('index.html', {'data': data})


def btq_logout(request):
    logout(request)
    ret_val = json.dumps({"logged_out": "true"})
    return HttpResponse(ret_val)


def btq_login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        print username
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                ret_val = json.dumps({"logged_in": "true"})
                return HttpResponse(ret_val)
            # Redirect to a success page.
            else:
                return HttpResponse(json.dumps({"logged_in": "not_active"}))
                # Return a 'disabled account' error message
        else:

            return HttpResponse(json.dumps({"logged_in": "false"}))
            # Return an 'invalid login' error message.
            # return TemplateResponse(request, 'registration/login.html')
    elif request.method == 'GET':
        form = AuthenticationForm()
        return render(request, 'registration/login.html', {'form': form})


def btq_register(request):
    """
    :param request: From the form
    :return: next page renderd
    """
    if request.method == 'POST':
        new_user_form = UserCreationForm(request.POST)
        if new_user_form.is_valid():
            new_user = new_user_form.save()
            ret_val = json.dumps({"registered": "true"})
            new_user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, new_user)
            return HttpResponse(ret_val)
        else:
            ret_val = json.dumps({"registered": "false"})
            return HttpResponse(ret_val)
    elif request.method == 'GET':
        form = UserCreationForm()
        return render(
            request,
            'registration/registration.html',
            {'form': form})
