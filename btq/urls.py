from django.conf.urls import patterns, include, url
from django.contrib import admin
from .views import *


urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'btq.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', home, name='home'),
    url(r'^is_logged_in/$', is_logged),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^index/$', index, name='index'),
    url(r'^favourites/$', get_user_favs, name='user_favourite'),
    # url(r'^login/$', btq_login, name='index'),
    url(r'^menu/(?P<vendor>\w{0,50})/$', get_menu, name="get_vendor_menu"),
    url(r'^login/$', btq_login, name="login_user"),
    url(r'^logout/$', btq_logout, name="logout_user"),
    url(r'^register/$', btq_register, name="register_user"),

)
