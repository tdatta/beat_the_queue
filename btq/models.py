__author__ = 'Tanmay'

from django.contrib.auth.models import User
from django.db import models
import timedelta
from datetime import datetime
# Create your models here.


class UserFavourite(models.Model):
    """
    Map for user and vendors
    """
    user_id = models.ForeignKey(
        User,
        related_name="ke_fav",
        limit_choices_to={'groups__name': "is_user"})
    vendor_id = models.ForeignKey(User, related_name="is_favoured_by",
                                  limit_choices_to={
                                      'groups__name':
                                      "is_vendor"})

    def __unicode__(self):
        return "User: "\
               + str(self.user_id.username)\
               + " ka fav hai " + str(self.vendor_id)

    class Meta:
        unique_together = ('user_id', 'vendor_id')


class Item(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class Menu(models.Model):
    item = models.ForeignKey(Item, related_name="menu_item")
    item_type = models.CharField(max_length=60)
    description = models.CharField(max_length=100)
    size = models.CharField(max_length=20)
    price = models.FloatField(default=0.0)
    vendor = models.ForeignKey(
        User,
        limit_choices_to={'groups__name': 'is_vendor'},
        related_name="vendor_ka_menu"
        )

    def __unicode__(self):
        return "Menu: " + str(self.vendor) + "item : " + str(self.item)

    class Meta:
        unique_together = ('item', 'vendor', 'description')


class Order(models.Model):
    quantity = models.IntegerField(default=1)
    item_id = models.ManyToManyRel(Item)
    user = models.ForeignKey(
        User,
        limit_choices_to={'groups__name': "is_user"},
        related_name="user_ka_order"
        )
    vendor = models.ForeignKey(
        User,
        limit_choices_to={'groups__name': "is_vendor"},
        related_name="vendor_ke_liye_order"
        )
