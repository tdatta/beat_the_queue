__author__ = 'Tanmay'
from django.contrib import admin

from .models import *


# Register your models here.

admin.site.register(UserFavourite)
admin.site.register(Menu)
admin.site.register(Item)
#admin.site.register(VendorMenu)           
admin.site.register(Order)
